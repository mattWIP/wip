window.slate = window.slate || {};
window.theme = window.theme || {};

/*================ Slate ================*/
// =require slate/a11y.js
// =require slate/cart.js
// =require slate/utils.js
// =require slate/rte.js
// =require slate/sections.js
// =require slate/currency.js
// =require slate/images.js
// =require slate/variants.js

/*================ Sections ================*/
// =require sections/product.js
// =require sections/header.js
// =require sections/megamenu.js
// =require sections/cart-drawer.js
// =require sections/slideshow.js
// =require sections/creativeCollection.js
// =require sections/instagram-feed.js

/*================ Templates ================*/
// =require templates/customers-addresses.js
// =require templates/customers-login.js

/*======= Vendor Includes =========*/
// =require vendor/cart.min.js
// =require vendor/jquery-2.2.3.min.js
// =require vendor/slick.min.js
// =require vendor/velocity.js
// =require vendor/instafeed.min.js
// =require vendor/imagesLoaded.js


  function isScrolledIntoView(elem, factor) {
      factor = factor || .8;
      var docViewTop = $(window).scrollTop();
      var docViewBottom = docViewTop + $(window).height();

      var elemTop = $(elem).offset().top;
      var elemBottom = elemTop + ($(elem).height()*factor);

      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
  }


$(document).ready(function() {
  var sections = new slate.Sections();
  sections.register('product', theme.Product);
  sections.register('header',theme.Header);
  sections.register('megamenu', theme.megamenu);
  sections.register('cartDrawer', theme.cart);
  sections.register('slideshow', theme.slideshow);
  sections.register('creative-collection', theme.creativeCollection);
  sections.register('instagram', theme.Instagram);
  // Common a11y fixes
  slate.a11y.pageLinkFocus($(window.location.hash));

  $('.in-page-link').on('click', function(evt) {
    slate.a11y.pageLinkFocus($(evt.currentTarget.hash));
  });

  // Target tables to make them scrollable
  var tableSelectors = '.rte table';

  slate.rte.wrapTable({
    $tables: $(tableSelectors),
    tableWrapperClass: 'rte__table-wrapper',
  });

  // Target iframes to make them responsive
  var iframeSelectors =
    '.rte iframe[src*="youtube.com/embed"],' +
    '.rte iframe[src*="player.vimeo"]';

  slate.rte.wrapIframe({
    $iframes: $(iframeSelectors),
    iframeWrapperClass: 'rte__video-wrapper'
  });

  // Apply a specific class to the html element for browser support of cookies.
  if (slate.cart.cookiesEnabled()) {
    document.documentElement.className = document.documentElement.className.replace('supports-no-cookies', 'supports-cookies');
  }

  var animations = {
      fade: '.js-fade',
      zoom: '.js-zoom',
      slide: '.js-slide',
      creative: '.creative__collection-image',
      insta: '.post.before'
  };

  // Window Scrolls into view function
  $(window).on("scroll", function() {
      let animationFade = $(animations.fade);
      let animationZoom = $(animations.zoom);
      let animationSlide = $(animations.slide);
      let animationCreative = $(animations.creative);
      let animationInsta = $(animations.insta);

      animationFade.each(function() {
          let $elm = $(this);
          if (isScrolledIntoView($elm)) {
              $elm.removeClass('js-fade');
          }
      });

      animationZoom.each(function() {
          let $elm = $(this);
          if (isScrolledIntoView($elm)) {
              $elm.removeClass('js-zoom');
          }
      });

      animationSlide.each(function() {
          let $elm = $(this);

          if (isScrolledIntoView($elm)) {
              $elm.removeClass('js-slide');
          }
      });

      animationInsta.each(function() {
          let $elm = $(this);

          if (isScrolledIntoView($elm, 0.3)) {
              $elm.removeClass('before');
          }
      });

  }); // end of window scroll

});



