/*!
 * modernizr.min.js
 */
// =require vendor/modernizr.min.js

/*!
 * jquery-2.2.3.min.js
 */
// =require vendor/jquery-2.2.3.min.js

/*!
 * Slick Slider JS
 */
// =require vendor/slick.min.js


 /* Introduce Cart.JS
  *	
  */

 // =require vendor/cart.min.js

/*!
 *  Handlebars.js
 */
// =require vendor/Handlebars.js


/*!
 *	Velocity.js v2.0.0
 */
// =require vendor/velocity.js

/*!
 *	Instafeed.min.js 1.9.3
 */
// =require vendor/instafeed.min.js

/*!
 *	ImagesLoaded 4.1.4
 */
// =require vendor/imagesLoaded.js



// Attempts to preserve comments that likely contain licensing information,
// even if the comment does not have directives such as `@license` or `/*!`.
//
// Implemented via the [`uglify-save-license`](https://github.com/shinnn/uglify-save-license)
// module, this option preserves a comment if one of the following is true:
//
// 1. The comment is in the *first* line of a file
// 2. A regular expression matches the string of the comment.
//    For example: `MIT`, `@license`, or `Copyright`.
// 3. There is a comment at the *previous* line, and it matches 1, 2, or 3.
