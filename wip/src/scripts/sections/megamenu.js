/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
   * @namespace product
 */

theme.megamenu = (function() {

  var selectors = {
    container: '[data-mega-menu]',
    nav: '.header_navigation nav',
    sticky: '[data-stick]'
  };

  /**
   * Product section constructor. Runs on page load as well as Theme Editor
   * `section:load` events.
   * @param {string} container - selector for the section container DOM element
   */
  function megamenu(container) {
    this.$container = $(container);
    let greater = $(container).parent();
    let targetAttr = $(container).attr('data-target');

    if( targetAttr.length ){
      let target = $('li#' + targetAttr).addClass('has-mega');
      greater.appendTo(target).show().parent().siblings().addClass('has-mega');
      let header = greater.closest('header.main_header').parent();
      let dataStick = $(selectors.sticky);

    let heightHeader = (greater.closest('header.main_header').height()) *0.5;

    greater.closest('.header__element.header_navigation').css('align-self','flex-end');

      $('ul.site-nav.mega li.has-mega').each(function(i){
          let halfHeight = $(this).height() * 0.5;
          let offset = heightHeader - halfHeight;
          $(this).css('padding-bottom',offset+'px');
      });

      if( dataStick.length ){ // this means stick nav is enabled. No need to assign position relative.
          let offset = header.height();
          $("body").css('margin-top', offset + 'px' );
      }else{ // this means position relative should be applied to header so mega menu goes full screen.
         // header.css('position','relative');
      }

    }

  }

  function preloadImages(){
    
  }

  function attach(){
    console.log('please');
  }

  megamenu.prototype = $.extend({}, megamenu.prototype, {
    /**
     * Event callback for Theme Editor `section:unload` event
     */
    onUnload: function() {
      this.$container.off(this.namespace);
    }
  });

  return megamenu;
})();
