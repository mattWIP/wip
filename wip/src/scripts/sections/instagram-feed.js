/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
   * @namespace product
 */

theme.Instagram = (function() {



  var selectors = {
    container: '[data-instagram]',
    instaFeed: '#instafeed',
    lightbox: '.lightbox-instagram',
    feedTemplate: '#feedTemplate',
    posts: '.post',
    postImg: '.post__img',
    arrows: '.instagram-control'
  };

  /**
   * Instagram section constructor. Runs on page load as well as Theme Editor
   * `section:load` events.
   * @param {string} container - selector for the section container DOM element
   */

  function build(data){
    // console.log(data.data);
    let posts = data.data;

      // HandlebarsJS template build
      var items = [],
      item = {},
      data = {},
      source = $("#feedTemplate").html();
      template = Handlebars.compile(source);

      for(var v=0; v<posts.length; v++){
            // console.log(posts[v]);
            let post = posts[v];

            item = {
                caption: ((post.caption != null ) ? post.caption.text : null),
                username: post.user.username,
                userImg: post.user.profile_picture,
                id: post.id,
                img: post.images.standard_resolution.url,
                likes: post.likes.count,
                link: post.link,
                location: ((post.location != null ) ? post.location.name : null),
                tags: post.tags,
                type: post.type
            };

            items.push(item);
      }

      data = {
        items: items
      };

      $('#instafeed').addClass('doublefeed');
      $('#instafeed').append(template(data)).addClass('templated');
      // $('.lightbox-instagram').append(template(data)); THIS COULD BE USED FOR SCROLLABLE TO FILL USING TEMPLATE.
   }


  function Instagram(container) {
      var settings = {};
      this.$container = $(container);
      let mock = this.$container.data('mock');
      this.namespace = '#instafeed'
      let buildEvent = new Event('build');
      let elm = this.$container;
      let scrollable = false;
      let scrollFunction = null;
      let instagram = this;

      settings = {
          get: 'user',
          userId: this.$container.data('instagram-id'),
          accessToken: this.$container.data('token'),
          limit: this.$container.data('limit'),
          mock: this.$container.data('mock'),
          resolution: 'thumbnail',
          before: function(){

          },
          after: function(){
                  instagram.assign();
          },
          success: function(data){
              if(mock){
                  build(data);
              }
          }
      };

      if( this.$container.data('lightbox') == 'scrollable' ){
          scrollable = true;
      }

      this.gather(settings);

      if(mock){
        this.lightbox(scrollable);
        $(selectors.posts).on('needsRequest', function(){
            console.log('make requests');
        });


        $(selectors.arrows).on('click', function(){ instagram.arrowsFill($(this).attr('data-target')) });

        $(window.document).on('click', selectors.posts ,function(){
              let index = $(this).index();
              console.log(index);
              if(scrollable){
                  instagram.scrollFill();
              }else{
                  instagram.arrowsFill(index);
              }
        }); 
      }
  }


   Instagram.prototype = $.extend({}, Instagram.prototype, {

    assign: function(){
          let posts = $('.post');
          let ids = $('span.id-span');
          let functions = this;

          ids.each(function(){
              let id = $(this).data('id');
              let product = $(this).data('product');

              if(product){
                  let $elm = $('.post[data-id="'+id+'"]');
                  let getURL = '/products/' + product + '.json';
                  let productURL = '/products/' + product;
                  let array = {};

                  $('.post[data-id="'+id+'"]').attr('data-product', product);              
                  $.get(getURL, function(data, status){
                    // console.log(data);
                      array = {
                          id: data.product.id,
                          image: data.product.image,
                          title: data.product.title,
                          url: productURL
                      };

                      functions.pull(array, $elm);
                  });
                }


          });
    },

    pull: function(array, $el){
          // console.log(array);
          $el.attr({
            'data-product-id': array.id,
            'data-product-image': array.image.src,
            'data-product-title': array.title,
            'data-product-url': array.url
          });
    },

    gather: function(settings){
        var feed = new Instafeed(settings);
        feed.run();
    },

    lightbox: function(scrollable){
      let div = $("<div>", { class:'lightbox-instagram'});
      
      if(scrollable){ // Scrollable Lightbox
          
      }else{ // Arrows lightbox
          let instagramWindow = $("<div>", { class: 'inner-window' });
          let img = $("<img>", { class: 'box-img' });
          let btn = $("<button>", { class: 'instagram-control' });
          let next = btn.clone().addClass('next');
          let prev = btn.clone().addClass('prev');

        let shopWindow = $("<div>", { class: 'instagram-shop'});
        let shopImg = $("<img>", { class: 'shopImg'});
        let shopTitle = $("<h3>", { class: 'shopTitle '});
        let shopLink = $("<a>", { class: 'shopLink'});

        shopWindow.append(shopImg, shopTitle);


          instagramWindow.append(img, next, prev, shopWindow);
          div.append(instagramWindow);
      }

     div.on('click', function(e){
        if($(e.target).hasClass('overlay')){
            $(e.target).removeClass('overlay');
        }
     });

      div.appendTo("body");
    },

    arrowsFill: function(i){
        let target =  $('.post:eq(' + i +')');
        let id = target.data('id');
        let img = target.find('img').attr('src');
        let lightbox = $('.lightbox-instagram');
        let lightboxWindow = $('.lightbox-instagram .inner-window');
        let lightboxImg = lightboxWindow.find('.box-img');
        let next = $('.instagram-control.next');
        let prev = $('.instagram-control.prev');
        let postLength = $('.post').length;
        if( target.data('product')){
            console.log('has product');

            let inImg = $('.shopImg');
            let inTitle = $('.shopTitle');

            let details = {
                productID: target.data('product-id'),
                img: target.data('product-image'),
                title: target.data('product-title'),
                url: target.data('product-url')
            }

            inImg.attr('src',details.img);
            inTitle.text(details.title);
            //shopLink.attr('href', details.url);
            lightbox.addClass('shop');
            
        }else{
            lightbox.removeClass('shop');
            console.log('no product');
        }


        lightboxImg.attr('src',img);
        lightbox.addClass('overlay');

        let num = parseInt(i);
        let nextNum =( (num+1>=postLength) ? 0 : num+1);
        let prevNum = ( (num-1<0) ? postLength-1 : num-1 );

        next.attr('data-target', nextNum );
        prev.attr('data-target', prevNum );
    },

    scrollFill: function(){
        console.log('scroll');
    },

    onSelect: function(){
      console.log('select instafeed');
      $('#instafeed').addClass('editing');
    },

    onDeselect: function(){
      
    },

    onReorder: function(){

    },

    onBlockSelect: function(){

    },

    onBlockDeselect: function(){

    },

    onUnload: function() {
      this.$container.off(this.namespace);
    },

    watchers: function(elm){
      elm.imagesLoaded()
        .always( function( instance ) {
          console.log('all images loaded');
        })
        .done( function( instance ) {
          console.log('all images successfully loaded');
        })
        .fail( function() {
          console.log('all images loaded, at least one is broken');
        })
        .progress( function( instance, image ) {
          var result = image.isLoaded ? 'loaded' : 'broken';
          console.log( 'image is ' + result + ' for ' + image.img.src );
        });
    }
  });

  return Instagram;
})();
