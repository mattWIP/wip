/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
   * @namespace product
 */

theme.slideshow = (function() {

  var selectors = {
    container: '[data-slideshow]',
    lazy: '[data-slick-lazy]'
  };

  /**
   * Product section constructor. Runs on page load as well as Theme Editor
   * `section:load` events.
   * @param {string} container - selector for the section container DOM element
   */
  function slideshow(container) {
      var settings = {};

      this.$container = $(container);

      settings = {
        accessibility: this.$container.data('accessibility'),
        adaptiveHeight: this.$container.data('adaptiveheight'),
        autoplay: this.$container.data('autoplay'),
        autoplaySpeed: this.$container.data('autoplayspeed'),
        arrows: this.$container.data('arrows'),
        draggable: this.$container.data('draggable'),
        infinite: this.$container.data('infinite'),
        zIndex: 2
      }

      this.$container.on('init',this.init);
      this.$container.on('lazyLoad', this.lazyLoad);

      this.activate(settings);
  }


  slideshow.prototype = $.extend({}, slideshow.prototype, {
    /** 
     * Event callback for Theme Editor `section:unload` event
     */

    activate: function(settings){
        this.$container.slick(settings);
    },

    init: function(slick){
        //console.log(slick);
        console.log($(selectors.lazy).length);
        if( $(selectors.lazy).length ){
           $(selectors.container).trigger('lazyLoad');
        }
        $(selectors.container).parent().velocity({opacity: 1}, 1000);
    },

    lazyLoad: function(){
        $(selectors.lazy).each(function(i){
          let $img = $(this);
          let src = $img.data('src');
          $img.attr('src',src);

        });
    },

    onSelect: function(){
     
    },

    onDeselect: function(){
      
    },

    onReorder: function(){

    },

    onBlockSelect: function(){

    },

    onBlockDeselect: function(){

    },

    onUnload: function() {
      this.$container.off(this.namespace);
    }
  });

  return slideshow;
})();
