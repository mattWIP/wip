/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
   * @namespace product
 */

theme.creativeCollection = (function() {



  var selectors = {
    container: '[data-creative-images]',
    imgs: '[data-creative-images]',
    img: '[data-creative-image]'
  };

  /**
   * Product section constructor. Runs on page load as well as Theme Editor
   * `section:load` events.
   * @param {string} container - selector for the section container DOM element
   */
  function creativeCollection(container) {
      var settings = {};

      this.$container = $(container);
      this.$container.on('intoView', this.centerImg);
      animationWatch(this.$container);

  }


  function animationWatch(elm){
    $(window).on("scroll",function(){
        let count = $(elm).find('img').length;
        let $imgs = $(elm).find('img');
       
      $imgs.each(function(){
          let img = $(this);
          let parent = img.parent();
          if( isScrolledIntoView(parent, .2) ){
             img.addClass('animated');
          } 
       });
    });    
  }


  creativeCollection.prototype = $.extend({}, creativeCollection.prototype, {
    /** 
     * Event callback for Theme Editor `section:unload` event
     */

    activate: function(settings){
        this.$container.slick(settings);
    },

    centerImage: function(){
        console.log('lets center the image.');
    },

    init: function(slick){
        //console.log(slick);
        console.log($(selectors.lazy).length);
        if( $(selectors.lazy).length ){
           $(selectors.container).trigger('lazyLoad');
        }
        $(selectors.container).parent().velocity({opacity: 1}, 1000);
    },

    lazyLoad: function(){
        $(selectors.lazy).each(function(i){
          let $img = $(this);
          let src = $img.data('src');
          $img.attr('src',src);

        });
    },

    onSelect: function(){
     
    },

    onDeselect: function(){
      
    },

    onReorder: function(){

    },

    onBlockSelect: function(){

    },

    onBlockDeselect: function(){

    },

    onUnload: function() {
      this.$container.off(this.namespace);
    },

    scrolledIntoView: function(elem){
      console.log('this scroll');
    }
  });

  return creativeCollection;
})();
