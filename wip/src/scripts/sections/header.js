/**
 * Header Section Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Header Section.
 *
   * @namespace header
 */

theme.Header = (function() {

  var selectors = {
    autoSearch: '[data-autosearch]',
    searchContainer: '[data-search-container]',
    searchForm: '[data-search-form]',
    searchInput: '[data-search-input]',
    searchBtn: '[data-search-button]',
    searchClose: '[data-search-close]',
    container: 'header.main_header',
    stickyNav: '[data-stick]'
  };

 /**
  * Private Variables Below
  *
  */

   let searchType = $(selectors.searchContainer).attr('data-style');

  /**
   * Product section constructor. Runs on page load as well as Theme Editor
   * `section:load` events.
   * @param {string} container - selector for the section container DOM element
   */
  function Header(container) {
    this.$container = $(container);
  }

  function stick(){
    let $body = $("body");
    let $nav = $(selectors.stickyNav);
    let navHeight = $nav.height();

    $body.css('margin-top', navHeight + 'px');

    $nav.css({
      'position': 'fixed',
      'top': '0px',
      'left': '0px',
      'right': '0px',
      'width':'100%',
      'z-index': '98',
      'background': 'white' 
    });
  }

  function autoComplete(){
        // Current Ajax request.
    var currentAjaxRequest = null;
    // Grabbing all search forms on the page, and adding a .search-results list to each.
    var searchForms = $('form[action="/search"]').each(function() {
      // Grabbing text input.
      var input = $(this).find('input[name="q"]');
      // Adding a list for showing search results.
    var offSet = $('header.main_header.grid').innerHeight() + input.height();
    $('<ul class="search-results"></ul>').css( { 'position': 'absolute', 'left': '30px', 'top': offSet + 'px' } ).appendTo($(this)).hide();    
      // Listening to keyup and change on the text field within these search forms.
      input.attr('autocomplete', 'off').bind('keyup change', function() {
        // What's the search term?
        var term = $(this).val();
        // What's the search form?
        var form = $(this).closest('form');
        // What's the search URL?
        var searchURL = '/search?type=product&q=' + term;
        // What's the search results list?
        var resultsList = form.find('.search-results');
        // If that's a new term and it contains at least 3 characters.
        if (term.length > 2 && term != $(this).attr('data-old-term')) {
          // Saving old query.
          $(this).attr('data-old-term', term);
            let offSet = $('header.main_header.grid').innerHeight() + input.height();
          $('ul.search-results').css('top', offSet + 'px');
          // Killing any Ajax request that's currently being processed.
          if (currentAjaxRequest != null) currentAjaxRequest.abort();
          // Pulling results.
          currentAjaxRequest = $.getJSON(searchURL + '&view=json', function(data) {
            // Reset results.
            resultsList.empty();
            // If we have no results.
            if(data.results_count == 0) {
              // resultsList.html('<li><span class="title">No results.</span></li>');
              // resultsList.fadeIn(200);
              resultsList.hide();
            } else {
              // If we have results.
              $.each(data.results, function(index, item) {
                var link = $('<a></a>').attr('href', item.url);
                link.append('<span class="thumbnail"><img src="' + item.thumbnail + '" /></span>');
                link.append('<span class="title">' + item.title + '</span>');
                link.wrap('<li></li>');
                resultsList.append(link.parent());
              });
              // The Ajax request will return at the most 10 results.
              // If there are more than 10, let's link to the search results page.
              if(data.results_count > 10) {
                resultsList.append('<li class="result__item"><span class="title"><a href="' + searchURL + '">See all results (' + data.results_count + ')</a></span></li>');
              }
              resultsList.fadeIn(200).css('z-index','2');
            }        
          });
        }
      });
    });
    // Clicking outside makes the results disappear.
    $('body').bind('click', function(){
      $('.search-results').hide();
    });
  }


  function watchers(){
    /**
     *  Search Watchers 
     */

    let searchJS = true;

    switch(searchType){
      case 'standard':
          //console.log('standard search');
          searchJS = false;
          break;
      case 'expand':
          //console.log('expand search');
          break;
      case 'fade':
          //console.log('fade search');
          $(selectors.searchInput).insertAfter($(selectors.searchBtn));
          break;
      case 'bar':
          //console.log('bar search');
          $(selectors.container).css('position','relative');
          break;
      case 'overlay':
          console.log('full overlay search');
          $(selectors.searchInput).wrap("<div id='inputContainer'></div>");
          break;
      default:
          console.log('none');
    }// End of Switch

    if(searchJS){ // No need for watchers if using standard search bar.
      let open = false;

       $(selectors.searchBtn).on("click", function(e){
          if(open){
            // Search submit
          }else{
            e.preventDefault();
            // Open search Input for typing
            let $form = $(selectors.searchForm); 
            $form.toggleClass('opened');
            open = true;

            if(searchType == 'overlay'){
              let $modal = $('#inputContainer');
                slate.a11y.trapFocus({
                  $container: $modal,
                  namespace:'modal',
                  $elementToFocus: $modal.find('input[type="search"]')
                });
            }
            if(searchType == 'bar'){
              let $modal = $('form.opened');

              slate.a11y.trapFocus({
                $container: $modal,
                namespace: 'bar',
                $elementToFocus: $modal.find('input[type="search"]')
              });

            }
          }
       });// End of Button on Click

       $(selectors.searchClose).on("click", function(e){
          e.preventDefault();
          let $form = $(selectors.searchForm);
          $form.toggleClass('opened');
          open = false;

          if(searchType == 'overlay'){
            let $modal = $('#inputContainer');
              slate.a11y.removeTrapFocus({
                $container: $modal,
                namespace: 'modal'
              });
          }
          if(searchType == 'bar' ){
            let $modal = $('li#search_container form');
              slate.a11y.removeTrapFocus({
                $container: $modal,
                namespace: 'bar'
              });
          }
       }); // End of Close on Click
    }// end of searchJS IF statement


  } // end of watchers function

  /**
   *  This will detect what type of search functions
   *  we will require for the selection choice.
   *
   *
   */ 


  Header.prototype = $.extend({}, Header.prototype, {
   /**
     * Event callback for Theme Editor `section:unload` event
     */
    onUnload: function() {
      // this.$container.off(this.namespace);
    }
  });

  if( $(selectors.autoSearch).length ){
        autoComplete();
  }
  
  if( $(selectors.stickyNav).length ){
        stick();
  }

  watchers();
  return Header;
})();
